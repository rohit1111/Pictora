import React, {Component} from 'react';
import NetInfo from '@react-native-community/netinfo';
// import { Cache } from "react-native-cache";
import AsyncStorage from '@react-native-community/async-storage';
import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  ToastAndroid,
  Platform,
  Image,
  Dimensions,
} from 'react-native';
import CameraComponent from './utility/CameraComp';
import getGeoLocation from './utility/getGeoLocation';
import Blink from './utility/Blink';

// const cache = new Cache({
//   namespace: "pictora",
//   policy: {
//       maxEntries: 1
//   },
//   backend: AsyncStorage,
// });

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  mainText: {
    fontSize: 25,
    color: 'black',
    textAlign: 'center',
    margin: 10,
  },
  uploadButton: {
    backgroundColor: 'blue',
    borderRadius: 20,
  },
  uploadButtonText: {
    fontSize: 14,
    color: 'white',
    margin: 15,
  },
  buttonContainerView: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignSelf: 'center',
  },
  buttonView: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    margin: 10,
  },
  buttonText: {
    fontSize: 20,
    color: 'white',
  },
});

class Pictora extends Component {
  constructor(props) {
    super(props);
    this.state = {
      width: 1,
      height: 1,
      imageRes: '',
      onPreview: false,
      geoLocation: null,
      isConnected: false,
      uploadStatus: false,
    };
    this.networkCheck = null;
    // this.netSubscribe = null;
    this.interval = null;
  }

  async componentDidMount() {
    // Here we ping https://www.google.com/ to check for internet.
    // As netinfo is not giving right information
    // We ping for every 5 sec and it will send conection status
    // to access geo loaction here it utility func which first grant the location access permission
    // and we are storing it in state and add it to the image data and send it to server
    this.xmlPing();
    getGeoLocation()
      .then(geoRes => {
        this.setState({geoLocation: geoRes});
      })
      .catch(e => {
        if (Platform.OS === 'android') {
          ToastAndroid.show('Unable to access geo location', ToastAndroid.LONG);
        }
      });
  }

  // Function to check internet status, it pings https://www.google.com/ and check if reachable
  xmlPing = async () =>
    new Promise((resolve, reject) => {
      fetch('http://www.google.com', {
        method: 'GET',
      })
        .then(() => {
          this.setState({ isConnected: true });
          resolve(true);
        })
        .catch(() => {
          this.setState({ isConnected: false });
          reject(false);
        });
      setTimeout(() => {
        resolve(false);
        this.setState({ isConnected: false });
      }, 1000);
    });

  componentWillUnmount() {
    // clear interval when this comp gets unmount
    clearInterval(this.networkChe);
    if (this.interval) {
      clearInterval(this.interval);
    }
  }

  // it removes the file info stored in async storage
  // we pass key to delete perticulr file info
  removeValue = async key => {
    try {
      await AsyncStorage.removeItem(`${key}`);
    } catch (e) {
      // remove error
    }
  };

  // To upload image to ngrok (localhost) server
  upoladImage = (imageRes, isState) => {
    const data = new FormData();
    data.append('name', 'avatar');
    data.append('fileData', {
      uri: imageRes.uri,
      type: imageRes.type,
      name: imageRes.fileName,
      coords: imageRes.geoCoords,
    });
    const config = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      body: data,
    };
    fetch('https://23dfc30e4f25.ngrok.io/' + 'upload', config)
      .then(() => {
        clearInterval(this.networkChe);
        if (Platform.OS === 'android') {
          ToastAndroid.show(
            'Image successfuly uploaded to server',
            ToastAndroid.LONG,
          );
        }
        if (isState) {
          this.setState({imageRes: null});
        } else {
          this.removeValue('pending');
          clearInterval(this.interval);
        }
        this.setState({uploadStatus: false});
      })
      .catch(() => {
        clearInterval(this.interval);
        if (Platform.OS === 'android') {
          ToastAndroid.show('Upload failed', ToastAndroid.SHORT);
        }
        clearInterval(this.networkChe);
        this.setState({imageRes: null, uploadStatus: false});
      });
  };

  // THIS WILL BE CALLED WHEN WE PRESS UPLOAD IN IMAGE PREVIEW
  // here we check if internet is connected, if true we directly upload the image with image data
  // exist in state and if internet is not connected we add info to async storage to retry it
  // when internet is back
  onMediaSend = async () => {
    let isConnected = await this.xmlPing();
    this.setState({onPreview: false, uploadStatus: true});
    if (isConnected) {
      this.upoladImage(this.state.imageRes, true);
    } else {
      this.networkChe = setInterval(() => {
        this.xmlPing();
      }, 3000);
      const jsonObj = JSON.stringify(this.state.imageRes);
      AsyncStorage.setItem('pending', jsonObj);
      this.interval = setInterval(async () => {
        if (this.state.isConnected) {
          const value = await AsyncStorage.getItem('pending');
          if (value !== null) {
            this.upoladImage(JSON.parse(value), false);
          }
        }
      }, 7000);
    }
  };

  // it will call camera component, which checks the permission and if granted will open camera and 
  // when we capture and select it will callback
  // When we get response callback we add geo location to the image object
  openCamera = () => {
    CameraComponent()
      .then(value => {
        const imageObj = value.res;
        // Added geo coords to image data
        imageObj.geoCoords = this.state.geoLocation
          ? this.state.geoLocation
          : null;
        // to get image size for image preview
        Image.getSize(imageObj.uri, (width, height) => {
          this.setState({width, height});
        });
        this.setState({imageRes: imageObj, onPreview: true});
      })
      .catch(error => {
        // console.log(error);
      });
  };

  // This will be called when we pressed cancel on image preview
  onPreviewClose = () => {
    this.setState({onPreview: false});
    if (Platform.OS === 'android') {
      ToastAndroid.show('Upload canceled', ToastAndroid.LONG);
    }
  };

  render() {
    if (this.state.onPreview) {
      return (
        <View
          // eslint-disable-next-line react-native/no-inline-styles
          style={{
            flex: 1,
            backgroundColor: 'black',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Image
            // eslint-disable-next-line react-native/no-inline-styles
            style={{
              width: '100%',
              height:
                (Dimensions.get('window').width * this.state.height) /
                this.state.width,
            }}
            source={{uri: this.state.imageRes.uri}}
          />
          <View style={styles.buttonContainerView}>
            <TouchableOpacity
              style={styles.buttonView}
              onPress={() => this.onMediaSend()}>
              {/* <MaterialCommunityIcons active name="send" style={{ fontSize: scale(30), color: Theme.WHITE }} /> */}
              <Text style={styles.buttonText}>Upload</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.buttonView}
              onPress={() => this.onPreviewClose()}>
              {/* <MaterialCommunityIcons active name="window-close" style={{ fontSize: scale(35), color: Theme.WHITE }} /> */}
              <Text style={styles.buttonText}>Cancel</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }

    return (
      <View style={styles.mainContainer}>
        <Text style={styles.mainText}>Welcome to Pictora</Text>
        <Text style={[styles.mainText, {fontSize: 20}]}>
          Here you can upload your image to server
        </Text>
        {this.state.uploadStatus ? (
          <Blink children="Uploading Image" />
        ) : (
          <TouchableOpacity
            style={styles.uploadButton}
            onPress={() => this.openCamera()}>
            <Text style={styles.uploadButtonText}>Upload Image</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

export default Pictora;
