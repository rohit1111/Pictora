import ImagePicker from 'react-native-image-picker';
import {cameraPermission} from './RequestPermission';

const CameraComponent = () => {
  let obj = {};
  return new Promise((resolve, reject) => {
    try {
      cameraPermission()
        .then(() => {
          const options = {
            mediaType: 'photo',
            quality: 0.6,
            // not added scale because it is crashing in android
            maxWidth: 1500,
            maxHeight: 1200,
            storageOptions: {
              skipBackup: true,
              path: 'images',
            },
          };
          ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
              obj.res = '';
              obj.err = response.error;
              obj.flag = false;
              reject(obj);
            } else {
              obj.res = response;
              obj.err = '';
              obj.flag = false;
              resolve(obj);
            }
          });
        })
        .catch(e => {
          // user might rejected the camera access permission
        });
    } catch (err) {
      obj.res = '';
      obj.err = err;
      obj.flag = false;
      reject(obj);
    }
  });
};

export default CameraComponent;
