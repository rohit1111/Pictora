import Geolocation from 'react-native-geolocation-service';
import {accessLocationPermission} from './RequestPermission';

const getGeoLocation = () =>
  new Promise((resolve, reject) => {
    accessLocationPermission()
      .then(() => {
        Geolocation.getCurrentPosition(
          position => {
            // console.log(position);
            // this.geoLocation = position.coords;
            resolve(position.coords);
          },
          error => {
            // See error code charts below.
            // console.log(error.code, error.message);
            reject(error.message);
          },
          {enableHighAccuracy: true, timeout: 15000, maximumAge: 10000},
        );
      })
      .catch(e => {
        // user might have rejected the permission
      });
  });

export default getGeoLocation;
