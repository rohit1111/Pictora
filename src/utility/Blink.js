import React, {Component} from 'react';
import {Text, View, StyleSheet} from 'react-native';
import {AppRegistry} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#ecf0f1',
  },
  offlineText: {color: 'red'},
});
let blinkTimer = null;

class Blink extends Component {
  constructor(props) {
    super(props);
    this.state = {showText: true};
    console.log(props);
    // Toggle the state every second
    blinkTimer = setInterval(() => {
      this.setState({showText: !this.state.showText});
    }, 100);
  }

  componentWillUnmount() {
    clearInterval(blinkTimer);
  }

  render() {
    let display = this.state.showText ? this.props.children : ' ';
    return <Text style={styles.offlineText}>{display}</Text>;
  }
}

export default Blink;
