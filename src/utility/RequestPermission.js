import {Platform, PermissionsAndroid} from 'react-native';

const cameraPermission = () => {
  return new Promise(async (resolve, reject) => {
    try {
      if (Platform.OS !== 'ios') {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          resolve('success');
        } else {
          reject('not granted');
        }
      } else {
        resolve('success');
      }
    } catch (err) {
      reject('not granted');
    }
  });
};

const accessLocationPermission = async () => {
  return new Promise(async (resolve, reject) => {
    try {
      if (Platform.OS !== 'ios') {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          resolve('success');
        } else {
          reject('not granted');
        }
      } else {
        resolve('success');
      }
    } catch (err) {
      reject('not granted');
    }
  });
};

export { cameraPermission, accessLocationPermission };
