/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import Pictora from './src/Pictora';

const App: () => React$Node = () => {
  return (
    <>
      <Pictora />
    </>
  );
};

export default App;
